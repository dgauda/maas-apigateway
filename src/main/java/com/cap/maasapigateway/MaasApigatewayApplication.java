package com.cap.maasapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import com.cap.maasapigateway.filters.ErrorFilter;
import com.cap.maasapigateway.filters.PostFilter;
import com.cap.maasapigateway.filters.PreFilter;
import com.cap.maasapigateway.filters.RouteFilter;

@SpringBootApplication
@EnableZuulProxy
public class MaasApigatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaasApigatewayApplication.class, args);
	}
	
	@Bean
    public PreFilter preFilter() {
        return new PreFilter();
    }
    @Bean
    public PostFilter postFilter() {
        return new PostFilter();
    }
    @Bean
    public ErrorFilter errorFilter() {
        return new ErrorFilter();
    }
    @Bean
    public RouteFilter routeFilter() {
        return new RouteFilter();
    }

}
