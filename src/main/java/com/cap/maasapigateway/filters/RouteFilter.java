package com.cap.maasapigateway.filters;

import com.netflix.zuul.ZuulFilter;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.ROUTE_TYPE;

public class RouteFilter extends ZuulFilter {
 
  @Override
  public String filterType() {
    return ROUTE_TYPE;
  }
 
  @Override
  public int filterOrder() {
    return 1;
  }
 
  @Override
  public boolean shouldFilter() {
    return true;
  }
 
  @Override
  public Object run() {
   System.out.println("Inside Route Filter");
    return null;
  }
}
